const faqItems = document.querySelectorAll(".faq__item");

faqItems.forEach((faqItem) => {
  faqItem.addEventListener("click", (event) => {
    faqItem.classList.toggle("faq__item--active");
  });
});
